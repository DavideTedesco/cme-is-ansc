import("stdfaust.lib");
freq1 = hslider("freq1", 40.573,10,10000,0.1) : si.smoothq(2,0.3);
freq2 = hslider("freq2", 40.573,10,10000,0.1) : si.smoothq(2,0.3);
freq3 = hslider("freq3", 40.573,10,10000,0.1) : si.smoothq(2,0.3);
freq4 = hslider("freq4", 40.573,10,10000,0.1) : si.smoothq(2,0.3);

n =6;
risonatori1 = _ <: par(i,n,_) : fi.resonbp((freq1*0+0.2),33300,10), fi.resonbp((freq1*1+0.2),4000,10), fi.resonbp((freq1*2+0.2),4000,10), fi.resonbp((freq1*3+0.2),4000,10), fi.resonbp((freq1*4+0.2),4000,10), fi.resonbp((freq1*5+0.2),4000,10):> _;
risonatori2 = _ <: par(i,n,_) : fi.resonbp((freq2*0+0.2),33300,10), fi.resonbp((freq2*1+0.2),4000,10), fi.resonbp((freq2*2+0.2),4000,10), fi.resonbp((freq2*3+0.2),4000,10), fi.resonbp((freq2*4+0.2),4000,10), fi.resonbp((freq2*5+0.2),4000,10):> _;
risonatori3 = _ <: par(i,n,_) : fi.resonbp((freq3*0+0.2),33300,10), fi.resonbp((freq3*1+0.2),4000,10), fi.resonbp((freq3*2+0.2),4000,10), fi.resonbp((freq3*3+0.2),4000,10), fi.resonbp((freq3*4+0.2),4000,10), fi.resonbp((freq3*5+0.2),4000,10):> _;
risonatori4 = _ <: par(i,n,_) : fi.resonbp((freq4*0+0.2),33300,10), fi.resonbp((freq4*1+0.2),4000,10), fi.resonbp((freq4*2+0.2),4000,10), fi.resonbp((freq4*3+0.2),4000,10), fi.resonbp((freq4*4+0.2),4000,10), fi.resonbp((freq4*5+0.2),4000,10):> _;

process = risonatori1/(n*10),risonatori2/(n*10),risonatori3/(n*10),risonatori4/(n*10) :> _,_ : fi.dcblocker,fi.dcblocker :co.compressor_stereo(4,-6,0.001,0.001);
