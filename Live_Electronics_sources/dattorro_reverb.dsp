import("stdfaust.lib");
//===============================Dattorro Reverb============================
//==========================================================================

//-------------------------------`(re.)dattorro_rev`-------------------------------
// Reverberator based on the Dattorro reverb topology. This implementation does
// not use modulated delay lengths (excursion).
//
// #### Usage
//
// ```
// _,_ : dattorro_rev(pre_delay, bw, i_diff1, i_diff2, decay, d_diff1, d_diff2, damping) : _,_
// ```
//
// Where:
//
// * `pre_delay`: pre-delay in samples (fixed at compile time)
// * `bw`: band-width filter (pre filtering); (0 - 1)
// * `i_diff1`: input diffusion factor 1; (0 - 1)
// * `i_diff2`: input diffusion factor 2;
// * `decay`: decay rate; (0 - 1); infinite decay = 1.0
// * `d_diff1`: decay diffusion factor 1; (0 - 1)
// * `d_diff2`: decay diffusion factor 2;
// * `damping`: high-frequency damping; no damping = 0.0
//
// #### Reference
//
// <https://ccrma.stanford.edu/~dattorro/EffectDesignPart1.pdf>
//------------------------------------------------------------
declare dattorro_rev author "Jakob Zerbian";
declare dattorro_rev licence "MIT-style STK-4.3 license";

dattorro_rev(pre_delay, bw, i_diff1, i_diff2, decay, d_diff1, d_diff2, damping) =
    si.bus(2) : + : *(0.5) : predelay : bw_filter : diffusion_network <: ((si.bus(4) :> _,_) ~ (reverb_network : ro.cross(2)))
with {
    // allpass using delay with fixed size
    allpass_f(t, a) = (+ <: @(t),*(a)) ~ *(-a) : mem,_ : +;

    // input pre-delay and diffusion
    predelay = @(pre_delay);
    bw_filter = *(bw) : +~(mem : *(1-bw));
    diffusion_network = allpass_f(142, i_diff1) : allpass_f(107, i_diff1) : allpass_f(379, i_diff2) : allpass_f(277, i_diff2);

    // reverb loop
    reverb_network = par(i, 2, block(i)) with {
        d = (672, 908, 4453, 4217, 1800, 2656, 3720, 3163);
        block(i) = allpass_f(ba.take(i+1, d),-d_diff1) : @(ba.take(i+3, d)) : damp :
            allpass_f(ba.take(i+5, d), d_diff2) : @(ba.take(i+5, d)) : *(decay)
        with {
            damp = *(1-damping) : +~*(damping) : *(decay);
        };
    };
};


//-------------------------------`(re.)dattorro_rev_default`-------------------------------
// Reverberator based on the Dattorro reverb topology with reverb parameters from the
// original paper.
// This implementation does not use modulated delay lengths (excursion) and
// uses zero length pre-delay.
//
// #### Usage
//
// ```
// _,_ : dattorro_rev_default : _,_
// ```
//
// #### Reference
//
// <https://ccrma.stanford.edu/~dattorro/EffectDesignPart1.pdf>
//------------------------------------------------------------
declare dattorro_rev_default author "Jakob Zerbian";
declare dattorro_rev_default license "MIT-style STK-4.3 license";

dattorro_rev_default = dattorro_rev(0, 0.9995, 0.75, 0.625, 0.5, 0.7, 0.5, 0.0005);

//process = dattorro_rev_default;

//----------------------------------`(dm.)dattorro_rev_demo`------------------------------
// Example GUI for `dattorro_rev` with all parameters exposed and additional
// dry/wet and output gain control.
//
// #### Usage
//
// ```
// _,_ : dattorro_rev_demo : _,_
// ```
//
//------------------------------------------------------------
declare dattorro_rev_demo author "Jakob Zerbian";
declare dattorro_rev_demo license "MIT-style STK-4.3 license";

dattorro_rev_demo = _,_ <: dattorro_rev(pre_delay, bw, i_diff1, i_diff2, decay, d_diff1, d_diff2, damping),_,_:
    dry_wet : out_level
with {
    rev_group(x) = hgroup("[0] Dattorro Reverb",x);

    in_group(x) = rev_group(hgroup("[0] Input",x));
    pre_delay = 0;
    bw = in_group(vslider("[1] Prefilter [style:knob] [tooltip: lowpass-like filter, 0 = no signal, 1 = no filtering]",0.7,0.0,1.0,0.001) : si.smoo);
    i_diff1 = in_group(vslider("[2] Diffusion 1 [style:knob] [tooltip: diffusion factor, influences reverb color and density]",0.625,0.0,1.0,0.001) : si.smoo);
    i_diff2 = in_group(vslider("[3] Diffusion 2 [style:knob] [tooltip: diffusion factor, influences reverb color and density]",0.625,0.0,1.0,0.001) : si.smoo);

    fdb_group(x) = rev_group(hgroup("[1] Feedback",x));
    d_diff1 = fdb_group(vslider("[1] Diffusion 1 [style:knob] [tooltip: diffusion factor, influences reverb color and density]",0.8,0.0,1.0,0.001) : si.smoo);
    d_diff2 = fdb_group(vslider("[2] Diffusion 2 [style:knob] [tooltip: diffusion factor, influences reverb color and density]",0.625,0.0,1.0,0.001) : si.smoo);
    decay = fdb_group(vslider("[3] Decay Rate [style:knob] [tooltip: decay length, 1 = infinite]",0.99,0.0,1.0,0.001) : si.smoo);
    damping = fdb_group(vslider("[4] Damping [style:knob] [tooltip: dampening in feedback network]",0.002,0.0,1.0,0.001) : si.smoo);

    out_group(x) = rev_group(hgroup("[2] Output",x));
    dry_wet(x,y) = *(dry) + wet*x, *(dry) + wet*y
    with {
        wet = 0.5*(drywet+1.0);
        dry = 1.0-wet;
    };
    drywet = out_group(vslider("[1] Dry/Wet Mix [style:knob] [tooltip: -1 = dry, 1 = wet]",0.8,-1.0,1.0,0.01) : si.smoo);
    gain = out_group(vslider("[2] Level [unit:dB] [style:knob] [tooltip: Output Gain]", -6, -70, 40, 0.1) : ba.db2linear : si.smoo);
    out_level = *(gain),*(gain);
};

process = fi.highpass(8,60) : fi.lowpass(8,10000) <: dattorro_rev_demo : fi.dcblockerat(16),fi.dcblockerat(16);
