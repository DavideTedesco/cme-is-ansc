import("stdfaust.lib");
freq = hslider("freq", 40.573,10,10000,0.1) : si.smoothq(2,0.3);
n =6;
risonatori = _ <: par(i,n,_) : fi.resonbp((freq*0+0.2),33300,10), fi.resonbp((freq*1+0.2),4000,10), fi.resonbp((freq*2+0.2),4000,10), fi.resonbp((freq*3+0.2),4000,10), fi.resonbp((freq*4+0.2),4000,10), fi.resonbp((freq*5+0.2),4000,10):> _;
process = risonatori/(n*10) <: _,_ : fi.dcblocker,fi.dcblocker :co.compressor_stereo(4,-6,0.001,0.001);
