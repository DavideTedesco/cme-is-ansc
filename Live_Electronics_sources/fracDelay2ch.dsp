import ("stdfaust.lib");
amp = hslider("amp", 0.9, 0.000001,1,0.00000001) :si.smoo;
f0 = hslider("f0", 6, 1,200,1) :si.smoo;
suono = no.sparse_noise;
vol = hslider("vol", 0,0,1,0.0001);
slai = hslider("slai", 0.6, 0.5,0.99998,0.00000001) :si.smoo;

del = hslider("del (s)", 1, 0, 5,0.000001) :  ba.sec2samp : si.smoothq(2,0.4);
process = _*vol :
si.smooth(slai) :
fi.highpass(8,110) : fi.allpassn(1,-0.7) <:
de.fdelaylti(4,ma.SR*2,del), de.fdelaylti(4,ma.SR*2,del*1.273), de.fdelaylti(4,ma.SR*2,del*1.357), de.fdelaylti(4,ma.SR*2,del*1.417), de.fdelaylti(4,ma.SR*2,del*1.517), de.fdelaylti(4,ma.SR*2,del*1.673) :> _,_ :
co.compressor_stereo(4,-6,0.001,0.001) :
fi.dcblocker,fi.dcblocker;
