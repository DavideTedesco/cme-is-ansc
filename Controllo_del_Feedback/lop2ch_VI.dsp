import("stdfaust.lib");

freqlp = nentry("[1]freq lp", 18000, 1, ((96000/2)-1), 1): si.smoo;
filtersOrder = 6;

process = _, _ : fi.lowpass(filtersOrder,freqlp),fi.lowpass(filtersOrder,freqlp);
