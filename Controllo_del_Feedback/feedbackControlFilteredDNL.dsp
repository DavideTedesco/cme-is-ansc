import("stdfaust.lib");
//sliders
c1 = hslider("gainIngresso",0.8,0.01,0.998,0.00001) : si.smoo;
c2 = hslider("gainUscita",0.6,0.01,0.998,0.00001) : si.smoo;

filterFrequencyLow = hslider("filterFrequencyLow",3000,0.01,20000,0.00001) : si.smoo;
filterFrequencyHigh = hslider("filterFrequencyHigh",0.01,0.01,20000,0.00001) : si.smoo;

ratioPre = hslider("ratioPre",2,0.01,50,0.00001) : si.smoo;
thresholdPre =  hslider("thresholdPre",2,0.01,12,0.00001) : si.smoo;
attackPre = hslider("attackPre",(1/1000),(1/10000),5,0.00001) : si.smoo;
releasePre = hslider("releasePre",(1/100),(1/10000),5,0.00001) : si.smoo;

ratioPost = hslider("ratioPost",2,0.01,20,0.00001) : si.smoo;
thresholdPost =  hslider("thresholdPost",2,0.01,12,0.00001) : si.smoo;
attackPost = hslider("attackPost",(1/1000),(1/10000),(1/10),0.00001) : si.smoo;
releasePost = hslider("releasePost",(1/1000),(1/10000),(1/10),0.00001) : si.smoo;

delayLength = hslider("delayLength",10,0,(48000*10),1) : ba.sec2samp :  si.smoo;
cd = hslider("gainDelay",0.0001,0.0001,0.998,0.00001) : si.smoo;
cdf = hslider("gainDelayFeedback",0.0001,0.0001,0.998,0.00001) : si.smoo;
dnlAmount = hslider("dnlAmount",0.001,0.0001,0.998,0.00001) : si.smoo;

interpolationTime = 2048;
//global variables (change at compilation time)
maxDelay = ma.SR*10;
//parts
gainIngresso = _*c1 ;
passaAlto = _ : fi.highpassLR4(filterFrequencyHigh) ;
passaBasso = _ : fi.lowpassLR4(filterFrequencyLow) ;
compressorePreDelay = _ : co.compressor_mono(ratioPre, thresholdPre, attackPre, releasePre) ;
//delayConFeedbackWrong = _ <:(_ + de.delay(maxDelay, delayLength))~*(cd) ;
delayConFeedback = _ <: _ : +~(de.sdelay(maxDelay, interpolationTime, delayLength))*(cdf) : *(cd) ;
compressorePostDelay = _ : co.compressor_mono(ratioPost, thresholdPost, attackPost, releasePost) ;
gainUscita = _*c2 ;
out = fi.dcblocker : _ ;

dnl = ma.tanh;

process =   _ :
            gainIngresso <:
            ((_*(-dnlAmount) - dnl*dnlAmount))  :>
            passaAlto :
            passaBasso :
            //compressorePreDelay :
            //delayConFeedback :
            //compressorePostDelay :
            gainUscita :
            out <:
            _*(0),_;
