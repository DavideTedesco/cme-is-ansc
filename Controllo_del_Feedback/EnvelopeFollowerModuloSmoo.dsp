declare filename "EnvelopeFollowerModuloSmoo.dsp";
declare name "EnvelopeFollowerModuloSmoo";
import("stdfaust.lib");

att = hslider("Attack",0.0001,0.0001,4,0.0001);
rel = hslider("Release",0.149,0.0001,4,0.0001);

envelopeFollwerModuloSmoo =   _,_: _*(an.amp_follower_ar(att,rel) : ma.modulo(1) : si.smoo);

//TEST
//process = os.osc(333),_: envelopeFollwerModuloSmoo;

process = _,_: envelopeFollwerModuloSmoo,os.osc(0)*(0);
