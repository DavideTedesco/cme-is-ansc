# Controllo del Feedback

# Links utili
- [compressione su Wikipedia](https://en.wikipedia.org/wiki/Dynamic_range_compression)
- [compressione sito JOS](https://ccrma.stanford.edu/~jos/filters/Nonlinear_Filter_Example_Dynamic.html)
-
## Links utili FAUST

- [libreria dei ritardi](https://faustlibraries.grame.fr/libs/delays/#defdelayn)
- [libreria dei compressori](https://faustlibraries.grame.fr/libs/compressors/#cocompressor_mono)
- [sintassi di base in FAUST](https://faustdoc.grame.fr/manual/syntax/#recursive-composition)
