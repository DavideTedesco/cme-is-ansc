import ("stdfaust.lib");

//baseFDBK.dsp
//with parts derived by seam.moorer.lib from https://github.com/s-e-a-m/faust-libraries
comb(t,g) = (+ : de.delay(ma.SR,t-1))~*(g) : mem;
apf(t,g,x) = (x+_ : *(-g) <: _+x,_ : de.delay(ma.SR,t-1),_)~(0-_) : mem+_;
apfo(0,t,g,x) = x;
apfo(1,t,g,x) = apf(t,g,x);
apfo(n,t,g,x) = (x+_ : *(-g) <: _+x,_ : apfo(n-1,t,g),_ : de.delay(ma.SR,t-1),_)~(0-_) : mem+_;

g = hslider("g",0.01,0,0.999,0.001);
ct = hslider("tauCMB",1,1,1000,1);
at = hslider("tauAPF",1,1,10000,1);

process = comb(ct,g) : apf(at,1/(sqrt(2)));
