declare filename "feedbackControl_LP_HP_DNL_GRAIN_simple.dsp";
declare name "feedbackControl_LP_HP_DNL_GRAIN_simple";
import("stdfaust.lib");

//SLIDERS
//gains
c1 = hslider("[01]gainInput",0.8,0.01,0.998,0.00001) : si.smoo;
c2 = hslider("[02]gainOutput",0.6,0.01,0.998,0.00001) : si.smoo;
//filter frequencies
filterFrequencyLow = hslider("[03]filterFrequencyLowPass",1500,0.01,20000,0.00001) : si.smoo;
filterFrequencyHigh = hslider("[04]filterFrequencyHighPass",0.01,0.01,20000,0.00001) : si.smoo;
//compressors pre parameters
ratioPre = hslider("ratioPre",2,0.01,50,0.00001) : si.smoo;
thresholdPre =  hslider("thresholdPre",2,0.01,12,0.00001) : si.smoo;
attackPre = hslider("attackPre",(1/1000),(1/10000),5,0.00001) : si.smoo;
releasePre = hslider("releasePre",(1/100),(1/10000),5,0.00001) : si.smoo;
//compressors post parameters
ratioPost = hslider("ratioPost",2,0.01,20,0.00001) : si.smoo;
thresholdPost =  hslider("thresholdPost",2,0.01,12,0.00001) : si.smoo;
attackPost = hslider("attackPost",(1/1000),(1/10000),(1/10),0.00001) : si.smoo;
releasePost = hslider("releasePost",(1/1000),(1/10000),(1/10),0.00001) : si.smoo;
//delayparameters
delayLength = hslider("delayLength",10,0,(48000*10),1) : ba.sec2samp :  si.smoo;
cd = hslider("gainDelay",0.0001,0.0001,0.998,0.00001) : si.smoo;
cdf = hslider("gainDelayFeedback",0.0001,0.0001,0.998,0.00001) : si.smoo;
dnlAmount = hslider("dnlAmount",0.997,0.0001,0.998,0.00001) : si.smoo;
//nld parameters
drive = hslider("drive", 0,0,1, 0.0001) : si.smoo;
offset = hslider("offset", 0,(-2),2,0.01) : si.smoo;
//grainer parameters
grainerFreq = hslider("[05]grainerFreq", 0.001,0,70,0.001) : si.smoo;
grainerPhase = hslider("grainerPhase", 0,0,1,0.001) : si.smoo;
amountGrainer = hslider("amountGrainer", 0,0,1,0.001) : si.smoo;
amountGrainerSin = hslider("[06]amountGrainerSin", 0,0,1,0.001) : si.smoo;
amountGrainerSaw = hslider("amountGrainerSaw", 0,0,1,0.001) : si.smoo;
amountGrainerSquare = hslider("amountGrainerSquare", 0,0,1,0.001) : si.smoo;
amountGrainerTriangle = hslider("[07]amountGrainerTriangle", 0,0,1,0.001) : si.smoo;

//FIXED VALUES
interpolationTime = 2048;
//GLOBAL VARIABLES (change at compilation time)
maxDelay = ma.SR*10;

//PARTS
guadagnoIngresso = _*c1 ;
passaAlto = _ : fi.highpassLR4(filterFrequencyHigh) ;
passaBasso = _ : fi.lowpassLR4(filterFrequencyLow) ;
compressorePreDelay = _ : co.compressor_mono(ratioPre, thresholdPre, attackPre, releasePre) ;
//delayConFeedbackWrong = _ <:(_ + de.delay(maxDelay, delayLength))~*(cd) ;
delayConFeedback = _ <: _ : +~(de.sdelay(maxDelay, interpolationTime, delayLength))*(cdf) : *(cd) ;
compressorePostDelay = _ : co.compressor_mono(ratioPost, thresholdPost, attackPost, releasePost) ;
guadagnoUscita = _*c2 ;
dcBlocker = fi.dcblocker : _ ;
//non-linear distortion
dnl =  _ <: ((_*(1-dnlAmount) + ma.tanh*dnlAmount))  :> _;
dnlCubic = _ <: ((_*(1-dnlAmount) + (ef.cubicnl(drive, offset))*dnlAmount))  :> _;
//grainers
//process = (os.osc(44)+1)/2;
grainerSaw(amp,freq) = _ <: _*(1-amp),_*((os.lf_sawpos_phase(freq,grainerPhase)+1)/2)*amp :> _;
grainerSquare(amp,freq) = _ <: _*(1-amp),_*((os.lf_squarewavepos(freq)+1)/2)*amp :> _;
grainerTriangle(amp,freq) = _ <: _*(1-amp),_*((os.lf_trianglepos(freq)+1)/2)*amp :> _;
grainerNoise(amp,rate) = _ <: _*(1-amp),_*no.lfnoise0(rate)*amp :> _;
grainerSin(amp,freq) = _ <: _*(1-amp),_*((os.osc(freq)+1)/2)*amp :> _*1;

grainer(amp,freq) =  grainerSin(amp,freq) ;
//routing
router = _ <: _*(0),_ ;
//TESTs
//process = os.osc(200) : grainerSquare(amountGrainerSquare,grainerFreq) : fi.dcblocker;
//process = os.osc(200) : grainerTriangle(amountGrainerTriangle,grainerFreq) : fi.dcblocker;
//process = os.osc(200) : grainerSaw(amountGrainerSaw,grainerFreq) :fi.dcblocker;
//process = os.osc(200) : grainerSin(amountGrainerSin,grainerFreq) :fi.dcblocker;
n = 1;
//CANDIDATE
process =   _ :
            guadagnoIngresso :
            dnl :
            //dnlCubic :
            passaAlto :
            passaBasso :
            //compressorePreDelay :
            //delayConFeedback :
            //compressorePostDelay :
            guadagnoUscita <:
            grainerSin(amountGrainerSin,grainerFreq),
            //grainerSquare(amountGrainerSquare,grainerFreq),
            //grainerSaw(amountGrainerSaw,grainerFreq),
            grainerTriangle(amountGrainerTriangle,grainerFreq) :>
            //router :
            par(i,n,dcBlocker);
