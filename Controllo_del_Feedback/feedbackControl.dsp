import("stdfaust.lib");
//sliders
c1 = hslider("gainIngresso",0.8,0.01,0.998,0.00001) : si.smoo;
c2 = hslider("gainUscita",0.8,0.01,0.998,0.00001) : si.smoo;
filterFrequency = hslider("filterFrequency",20,0.01,8000,0.00001) : si.smoo;
ratioPre = hslider("ratioPre",2,0.01,12,0.00001) : si.smoo;
thresholdPre =  hslider("thresholdPre",2,0.01,12,0.00001) : si.smoo;
attackPre = hslider("attackPre",(1/1000),(1/10000),(1/10),0.00001) : si.smoo;
releasePre = hslider("releasePre",(1/1000),(1/10000),(1/10),0.00001) : si.smoo;
ratioPost = hslider("ratioPost",2,0.01,12,0.00001) : si.smoo;
thresholdPost =  hslider("thresholdPost",2,0.01,12,0.00001) : si.smoo;
attackPost = hslider("attackPost",(1/1000),(1/10000),(1/10),0.00001) : si.smoo;
releasePost = hslider("releasePost",(1/1000),(1/10000),(1/10),0.00001) : si.smoo;
delayLength = hslider("delayLength",10,0,(ma.MAX-1),1) : ba.sec2samp :  si.smoo;
cd = hslider("gainDelay",0.0001,0.0001,0.998,0.00001) : si.smoo;
cdf = hslider("gainDelayFeedback",0.0001,0.0001,0.998,0.00001) : si.smoo;
//global variables (change at compilation time)
maxDelay = 384000;
//parts
gainIngresso = _*c1 ;
passaAlto = _ : fi.highpassLR4(filterFrequency) ;
compressorePreDelay = _ : co.compressor_mono(ratioPre, thresholdPre, attackPre, releasePre) ;
//delayConFeedbackWrong = _ <:(_ + de.delay(maxDelay, delayLength))~*(cd) ;
delayConFeedback = _ <: _ : +~(de.delay(maxDelay, delayLength))*(cdf) : *(cd) ;
compressorePostDelay = _ : co.compressor_mono(ratioPost, thresholdPost, attackPost, releasePost) ;
gainUscita = _*c2 ;
out = _ ;

process =   _ :
            gainIngresso :
            passaAlto :
            compressorePreDelay :
            delayConFeedback :
            //compressorePostDelay :
            gainUscita :
            out :
            _;
