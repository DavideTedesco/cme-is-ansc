import("stdfaust.lib");

time = hslider("time", 0.3,0,5,0.001);
curve = hslider("curve (exp - lin)", 0.2,0,0.999999999,0.001);
interpolator = hslider("volume",0,0,0.9999,0.0001): si.smoothq(time,curve);
n = 4;
process = si.bus(n) : par(i,n,_*interpolator) :par(i,n,_);
