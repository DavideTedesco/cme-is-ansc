# Pedali

|Tipologia   |Marca   | Modello  | Costo |  Link|
|---|---|---|---|---|
|Mono Passivo   |Ernie Ball   |MVP Most Valuable Pedal   | 191 euro  |https://www.thomann.de/it/ernie_ball_mvp_most_valuable_pedal.htm |
|Mono Attivo   |Lehle   |Mono Volume   | 199 euro  |https://www.thomann.de/it/lehle_mono_volume.htm |
|Stereo Passivo   | Ernie Ball  | Volume Pedal 500K  |222 euro  |https://www.thomann.de/it/ernie_ball_volume_pedal_205k.htm |
|Stereo Attivo   |Lehle   |Stereo Volume   |262 euro  |https://www.thomann.de/it/lehle_stereo_volume.htm |
