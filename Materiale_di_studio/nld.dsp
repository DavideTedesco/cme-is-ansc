//taken from https://ccrma.stanford.edu/~jos/pasp/Cubic_Soft_Clipper.html
//https://ccrma.stanford.edu/~jos/pasp/Nonlinear_Distortion.html
//https://ccrma.stanford.edu/~jos/pasp/Nonlinear_Elements.html#sec:nonlin
//https://ccrma.stanford.edu/~jos/pasp/Software_Cubic_Nonlinear_Distortion.html
//https://ccrma.stanford.edu/realsimple/SimpleStrings/Nonlinear_Overdrive.html
//https://faustlibraries.grame.fr/libs/misceffects/#efcubicnl

import ("stdfaust.lib");

drive = hslider("drive", 0,0,1, 0.0001) : si.smoo;
offset = hslider("offset", 0,(-2),2,0.01) : si.smoo;
frequency = hslider("frequency", 207,0,20000,0.01) : si.smoo;
frequencyLFO = hslider("frequencyLFO", 100,0,20000,0.01) : si.smoo;

cubicnl(drive,offset) = *(pregain) : +(offset) : clip(-1,1) : cubic
with {
    pregain = pow(10.0,2*drive);
    clip(lo,hi) = min(hi) : max(lo);
    cubic(x) = x - x*x*x/3;
    postgain = max(1.0,1.0/pregain);
};

cubicnl_nodc(drive,offset) = cubicnl(drive,offset) : fi.dcblocker;

lfoffset = os.osc(frequencyLFO);
process = os.osc(frequency) <: cubicnl_nodc(drive,offset) ;

//process = os.osc(frequency) : softclipQuadratic;
