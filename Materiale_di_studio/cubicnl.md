https://github.com/grame-cncm/faustlibraries/blob/master/misceffects.lib


con il codice del cubicnl:

```
//---------------------`(ef.)cubicnl`-----------------------
// Cubic nonlinearity distortion.
// `cubicnl` is a standard Faust function.
//
// #### Usage:
//
// ```
// _ : cubicnl(drive,offset) : _
// _ : cubicnl_nodc(drive,offset) : _
// ```
//
// Where:
//
// * `drive`: distortion amount, between 0 and 1
// * `offset`: constant added before nonlinearity to give even harmonics. Note: offset
//    can introduce a nonzero mean - feed cubicnl output to dcblocker to remove this.
//
// #### References:
//
// * <https://ccrma.stanford.edu/~jos/pasp/Cubic_Soft_Clipper.html>
// * <https://ccrma.stanford.edu/~jos/pasp/Nonlinear_Distortion.html>
//------------------------------------------------------------
cubicnl(drive,offset) = *(pregain) : +(offset) : clip(-1,1) : cubic
with {
    pregain = pow(10.0,2*drive);
    clip(lo,hi) = min(hi) : max(lo);
    cubic(x) = x - x*x*x/3;
    postgain = max(1.0,1.0/pregain);
};

cubicnl_nodc(drive,offset) = cubicnl(drive,offset) : fi.dcblocker;

declare cubicnl author "Julius O. Smith III";
declare cubicnl license "STK-4.3";
declare cubicnl_nodc author "Julius O. Smith III";
declare cubicnl_nodc license "STK-4.3";
```

