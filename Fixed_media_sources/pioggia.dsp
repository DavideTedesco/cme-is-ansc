import("stdfaust.lib");

// Set the threshold for generating an impulse
threshold = 1.01;
maxdel =3000;
intdel =3330;
aN = 0.8;

amp = hslider("amp", 1.5, 0.000001,10,0.00000001);
f0 = hslider("f0", 6, 1,1000,0.0001);

// Generate a random noise signal
randomSignal1 = no.velvet_noise(amp, f0)+(no.velvet_noise(amp, f0) : @(ma.SR/6));// : re.mono_freeverb(0.0003,0.0007,0.002,0.0001);//:
randomSignal2 = no.velvet_noise(amp, f0-2)+(no.velvet_noise(amp, f0): @(ma.SR/6));
randomSignal3 = no.velvet_noise(amp, f0-1)+(no.velvet_noise(amp, f0): @(ma.SR/6));
randomSignal4 = no.velvet_noise(amp, f0-1)+(no.velvet_noise(amp, f0): @(ma.SR/6));

slai = hslider("slai", 0.956, 0.000001,0.99998,0.00000001) :si.smoo;
// Create an impulse when the random signal exceeds the threshold
impulse1 = randomSignal1  : si.smooth(slai) ;
impulse2 = randomSignal2  : si.smooth(slai) ;
impulse3 = randomSignal3  : si.smooth(slai) ;
impulse4 = randomSignal4 : si.smooth(slai) ;

// Output the impulse
process = impulse1,impulse2,impulse3,impulse4 :
fi.allpass_fcomb5(10024,(3000.5*f0)/1000,0.75),
fi.allpass_fcomb5(10024,(2000.5*f0)/1000,0.79),
fi.allpass_fcomb5(10024,(3000.5*f0)/1000,0.75),
fi.allpass_fcomb5(10024,(3000.5*f0)/1000,0.73) :
_,
(_ : de.delay(40000,235)),
(_ : de.delay(40000,237)),
(_ : de.delay(40000,243)) :
 fi.dcblocker,fi.dcblocker,fi.dcblocker,fi.dcblocker ;
