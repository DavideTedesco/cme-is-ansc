<CsoundSynthesizer>
<CsOptions>
; Setting the output file to silence.wav, 96 kHz sample rate, 16-bit wav format
-o silence.wav -r 96000 -W -f
</CsOptions>

<CsInstruments>

instr 1
    ; Generate silence for 5 seconds
    aOut = 0
    aOut1 = 0
    aOut2 = 0
    aOut3 = 0
    outs aOut, aOut1, aOut2, aOut3
endin

</CsInstruments>

<CsScore>
; Start instrument 1 at time 0 and play for 5 seconds
i 1 0 180
e
</CsScore>

</CsoundSynthesizer>

