import("stdfaust.lib");

// Set the threshold for generating an impulse
threshold = 1.01;
maxdel =3000;
intdel =3330;
aN = 0.8;

amp = hslider("amp", 0.9, 0.000001,1,0.00000001) :si.smoo;
f0 = hslider("f0", 6, 1,200,1) :si.smoo;

// Generate a random noise signal
randomSignal1 = no.velvet_noise(amp, f0);// : re.mono_freeverb(0.0003,0.0007,0.002,0.0001);//:
randomSignal2 = no.velvet_noise(amp, f0-2);
slai = hslider("slai", 0.956, 0.91,0.99998,0.00000001) :si.smoo;
// Create an impulse when the random signal exceeds the threshold
impulse1 = randomSignal1  : si.smooth(slai) ;
impulse2 = randomSignal2  : si.smooth(slai) ;

freqReson = hslider("freqReson", 80, 40,2000,1) :si.smoo;
// Output the impulse
process = impulse1,impulse2 : fi.resonbp(freqReson,343,12),fi.resonbp(freqReson-3,333,12) :
 _,(_ : de.delay(40000,10011)): fi.dcblocker,fi.dcblocker :
 co.compressor_stereo(4,-6,0.001,0.001);// : re.stereo_freeverb(0.3,0.1,0.31,333);
