# Appunti della lezione di giovedì 11 gennaio 2024

Esercizi ogni 15 giorni e intercalari anche via Skype con possibilità di test. Per gli esercizi.

Ottenere un piano di lavoro diversificato fra tutti.

Orari di lezione interna suddivisi in questa maniera:
- Vittoria e Manuela ore 15 -> aspetti anche di natura basilare
- a seguire Roberto e Davide


## Ascolto e visione dei campioni e delle FFT

_appunti di carta da riportare in digitale_

## Argomenti di base trattati
- Campionamento introduzione
- Quantizzazione introduzione
  - introduzione interi
  - introduzione virgola mobile
- Introduzione ADC e DAC
- Introduzione al foldover
- FFT
- segnali di controllo (asincrono) -> il resto
- segnali alla massima velocità (sincrono) -> calcoli in banda audio
- introduzione del tempo reale reale
  - modello di computazione a blocchi
- introduzione alle operazioni in Max
- introduzione all'Algebra di Boole
  - comparazione di valori

## Primo esercizio su Max

Da un onda sinusoidale a un'onda quadra!

Frammento di brano sottoposto a delle elaborazioni logiche con i segnali di controllo con variazioni più estreme.

## Secondo esercizio su Max

Lettura di un file audio.

## Esercizio

Figura di Chladni adeguata con attuatore puntiforme (da prendere al CRM), attuatore puntiforme con vite gommata, è utile la figura di Chladni. Si verifica a frequenze date la struttura dei nodi e dei ventri della tavola armonica.

Tavola armonica accoppiata all'intero contesto e non esprime se stessa da sola.

Prime prove da realizzare di ordine musicale che consistono in esercizio compositivo realizzato con soli arpeggi strutturati in una condizione in cui si hanno:
- accordi risonanti
- accordi stoppati

Articolare arpeggi il più veloce possibile.

Obiettivo: controllare il feedback che si produce dalla cassa armonica all'attuazione in quel punto.

Sensore -> Attuatore

In mezzo tra sensore e attuatore inseriamo:
- sensore
- moltiplicatore $c_1$ -> gain del sensore
- filtro passa alto -> protezione dalle lunghezze d'onda non controllabile da 10 Hz al di sotto della frequenza più grave
- compressore -> lavora su aspetti timbrici, mettendo apposto l'attacco, si cercano di ottenere attacchi dolci e pregnanti; ottenere una qualità d'attacco dello strumento
- ritardo -> aiuta a controllare il feedback e inseriamo una controreazione sul delay, per moltiplicare l'arpeggio in modo forte con moltiplicatore $c_d$ sulla controreazione, controllo del tempo di delay $t_d$, mantenendosi al di sotto dei 40/50 ms si ha un alone riverberante, per dare spessore all'arpeggio, riverberazione quando si stoppa l'arpeggio
- compressore -> protezione dal feedback in cui si realizza il suono per il risultato finale sull'attuatore, tempo d'attacco e compressione determina l'invadenza attuativa
- moltiplicazione
- pedale o potenziometro analogico -> successivamente
- moltiplicatore $c_2$
- attuatore
- chitarra

Arpeggio stoppato, serve a controllare le modalità di feedback, controllo legato alle modalità di esecuzione.

Durata massima 3 minuti, con osservazione che gli arpeggi devono progressivamente crescere in termini di frequenza fino a raggiungere posizioni al 12/13 tasto, con note scomode, con crescita progressiva in frequenza.

La crescita non è sempre lineare, si scrivono linee dritte andandoci per mezzo della curva.

Dalle 15 il 18 gennaio incontro con gli strumentisti.

## Suoni da cui partono gesti per Roberto

Coerenza sottesa e invalicabile, partire da un suono ed arrivare ad un'immagine di gesto.

Suoniforma: antecedente, core, conseguente

## Tipologie di partiture

Partitura di esecuzione.

Partitura di rappresentazione del suono.

Partitura di riproduzione del suono con simboli per la realizzazione degli algoritmi, etc...

Osservare partiture di Giuseppe Chiari.

## Libri

- Kandinsky - Punto Linea e Superficie
- Klee - Teoria dell'arte e della figurazione

## Materiale per attaccare l'attuatore

Creare un punto d'appoggio basato su spugna o polipropilene, comunque un materiale elastico, che aderisce bene sulla parte lignea, incollando la spugna con il biadesivo.

## Sensore piezoelettrico

Da vedere al CRM...
