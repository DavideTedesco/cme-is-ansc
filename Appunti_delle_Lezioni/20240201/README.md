# Appunti della lezione di giovedì 1mo febbraio 2024

## Materiali

Materiali più rigidi per il sensore piezoelettrico.

Inserire il sensore seguendo la linea della fibra.

Fotografia estensiva della trasmissione frequenziale dello strumento.

Si potrebbe avere una regione frequenziale privilegiata parallelamente.

Mentre ponendosi ortogonalmente si ha un'informazione più ampia di come si stanno trasmettendo le lunghezze d'onda nelle diverse fibre.

La grandezza delle fibre non deve essere molto diversa.

La trasmissione omogenea dell'energia è garantita dalla grandezza paragonabile delle fibre del piano armonico.

>Testo di Scienza dei materiali. Per lo studio dei piani armonici.

Benade -> sono considerati come risonatori, che come elementi vibrazionali con proprietà specifica.

Materiali isotropi grantiscono una rilevazione dell'energia paritetica in tutti i punti.

Ad esempio una lastra di acciaio inox è un trasmettitore coerente in base all'anatomia coerente del mezzo che si sta eccitando.

Un testo sostanziale su testi e tecniche di analisi vibrazionale: gruppo del DIMI di Roma Tre. Simulazione vibrazionale del materiali per gli aerei.

Umberto Jemma Roma Tre...

Oggetto che suonoava conteneva la partitura musicale, ovvero il planofono con i suoi tagli, i suoi angoli, le dimensioni e lo spessore davano la possibilità di dare una dimensione percepibile.

Puntiforme il punto di attuazione.

![1](1.png)

Le gobbe nere sono i punti di cancellazione della frequenza.

Gli angoli e i tagli determinano l'irradiazione delle frequenze.

![2](2.png)

Distinguere il progetto espressivo spaziale dell'opera.

Palpacelli aiutò Lupone a comporre le fibre del legno armonico della Val di Fiemme.

Chiamare Jemma, per poter capire come interagire con i materiali, per un controllo della vibrazione.

[Scienza e tecnologia dei materiali STM DIMI](http://www.stm.uniroma3.it/SitePages/Personal_page.aspx?IDP=4)
## Piezo

- Negativo -> 1
- Positivo -> 2

XLR quarta connessione di chassis. Riferimento di Ground, pubblicazioni AES indicazioni connessione con connessione di chassis.

## Algoritmo per il feedback

Compressori limiter.

Compressore post delay da rimuovere ed utilizzare quello del finale.

delay controreazione che va in ingresso delay, e il moltiplicatore

Retroazione serve a non andare ad infinito, mentre il delay serve a creare gli sfasamenti che nella parte acustica servono a non provocare una degenerazione dovuto al fatto che le relazioni di fase siano tutte positive.

Il delay serve ad andare in controfase con ciò che sta rientrando.

![delay_correzione](delay_correzione.png)

La migliore linea di delay l'ha trattata il progettista della Lexicon, ed il suo delay è articolato con articolazioni multiple.

## Partiture per Roberto

- Stockhausen -> Plus minus
- Kagel -> Exotica, Transizioni, Match, Acoustica

## Sensoristica per Roberto

[Sensore MIDI](https://www.midi3d.com/)

>Trovare altre periferiche e sensori per Roberto.

[Sensore con guanti SPECKTR](https://www.specktr.com/?lang=en)

[Sensore](https://en.global-dj.com/)

Leonello Tarabella ingegnerie che realizzò guanti per il controllo di molti parametri.

Esercizio per Roberto: sensoristica molto dettagliata e articolata per comprare i guanti.

Datasheet del sensore che ci permette di muovere e gestire l'andamento nel tempo dei parametri acquisiti dal sensore.

## Esercizi

Da ricevere dal Maestro Lupone.
