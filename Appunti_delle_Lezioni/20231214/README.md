# Appunti della lezione di giovedì 14 dicembre 2023

## 2 aree del gruppo 1
- compositori
- specialisti (impegno direzionato)

## Piano

- essere pronti per giugno
- dilazione verso luglio

## Specialisti
>Scritti teorici relativi anche ad una metodologia di ricerca.

### D

Incontro/scontro preliminare per capire come muoversi.

Si cresce arrichendosi delle idee degli altri.

Stabilire dei passi progressivi per immediate applicazioni di alcuni processi. Legate a queste tecnologie delle microesercitazioni compositive per realizzare ed avere un riscontro applicativo.

Un sensore/attuatore, è un mezzo e uno strumento con limiti e proprietà. I limiti vanno conosciuti e va applicato uno studio per permettere un intervento originale.

È possibile portare elementi in Studio anche se alcuni test si potranno e dovranno realizzare al CRM.

### R

Comprendere e capire l'attitudine dell'allievo.

## Compositori

logiche appropriate per Live Electronics e funzioni utili per la costruzione di acusmatici.

relazioni che vengono instaurate con gli altri suoni.

riconoscimento informativo fondamentale sul rumore, rumore che diviene un suo nuovo.

- aspetto metodologico
- con forme di esercitazione

- Agresti -> esercizi mirati ad una progressiva capacità di elaborazione degli algoritmi.
- Guerra -> capire come realizzare il percorso per mettere in gioco abilità dal punto di vista algoritmico

## Elementi investigativi

Alcuni elementi si stanno svelando ed attuando ultimamente.

Problema fisico-acustico legato alla diffusione del suono.

Desiderio di approfondimento e creazione all'interno dell'ambito di conoscenza.

## Domande del docente

Qualsiasi domanda è lecita. Cercare di allargare le nostre capacità.

## Gli allievi

### Manuela Guerra

- Registrazioni di suoni e modifica VST consapevole
- Qualcosa sempre legato a scrittura strumentale
- Ableton
- Max
- Stockhausen
- Eliane Radigue
- Mauro Lanza
- Fausto Romitelli

>Osservazione dello strumento per qualcosa di particolare.

---

Studio fisico degli strumenti: flauto, violino e violoncello.

### Roberto Mongardini

CSound, Max e Pure Data.

Spazializzazione.

LAC sala per la spazializzazione con diffusione con 20 diffusori con mixer tipo Acousmonium.

Figura dell'interprete elettroacustico.

Aspetto neurologico-cognitivo rispetto al Live Electronics; studi per strumento solo per la comprensione dello strumento esteso; elettronica come estensione dello strumento.

Brani per violoncello esteso.

Realizzazione di lavoro gestuale con tipi di processi che smuovono a livello neurologico meccanismi...

Studio delle immagini, con AI con tempo reale...

---

Gesto che non da informazioni su ciò che si può ottenere.

Analisi gestuale fatta in quale maniera? Affinchè l'interprete impari a gestire i suoi gesti, o indirizzato ad una logica compositiva (partitura gestuale) per indirizzarli a ciò che ho scritto?

Grado di ideterminzione su ciò che si scrive.

Progetto di ricerca sul Kansei.

Identificazione del gesto, quindi catalogazione e poi utilizzo.

Sforzo da artigiano per costruire gli utensili e poi la composizione vera e propria.

### Davide Tedesco

Bisogno di concretezza da musicista, richiudendolo all'interno di un percorso musicale.

Tipologie di Feedback:
- Feedback in aria; tipico esempio microfono altoparlante
- Feedback strutturale, che partecipa della vibrazione in termini meccanici, natura particolare e non più dipendente dal lobo di trasmissione del segnale; accoppiamento degli elementi elettroacustici realizza un elemento unico; modi di risonanza definiti dai modi di risonanza; nella natura fisica di un sistema risonante; la grandezza e la geometria dello strumento determinano la natura del Feedback che si genererà
- Feedback digitale, tipica dei filtri e delle controreazioni realizzati con processi di sintesi ed elaborazione del suono; tale feedback è in grado di fornire una gestione del segnale molto sofisticata

---

Andamento dinamico che importante controllare.

Attenzione alla saturazione del microfono e dell'altoparlante.

Onde quadre come stimolatori e controllori di altri segnali, tramite ad esempio il sintetizzatore Moog.

Onde quadre e saturazionismo dell'estetica musicale.


Processo analogico:
- viaggiare alla velocità della luce
- algoritmi per perfezionare, controllare e individuare elementi

Processo digitale:
- decorrellazione delle fasi

Strumento già provvisto di corde, altrimenti si tratta di un Planofono, ovvero piano vibrante.

Vincoli posti dallo strumentista sullo strumento, che determinano insieme all'utilizzo del filtraggio la vera aumentazione del sistema.

Un processo di feedback è un processo caotico ed il caos ha dei centri gravitazionali e si adatta creando un bacino.

Lunghezza d'onda grande, più il tempo di feedback aumenta.

30 dB -> da 60 dB abbiamo un'escursione dinamica di 30 dB.

Lavorare sulle scordature e sulle formanti.

Attuatore:
- attuatore a spinta -> frequenze per lunghezze d'onda brevi
- attuatore a shaker -> movimento più ampio che emette frequenze qpiù gravi

Passi:
1. indagine con filtri estremamente selettivi, molto risonanti per uscire con eccitazione a colpo sulla cassa per comprendere la risposta e l'equalizzazione delle energie (gestione con 6 filtri selettivi con reiezione di banda di 24 dB sulle frequenze di intonazione delle corde) vedere come la cassa risponde con singole corde; per evidenziare le emergenze ![1](1.png)
2. analisi con singolo impulso e treno di impulsi (con attuatore a pistone, che produce un contatto istantaneo con la chitarra) -> quasi come un pizzicato
3. impulsi con pollice dorso, impulsi con schicchera con unghia con spettro piatto (tutto in fase)

Per non rovinare l'attuatore inserire un generatore di impulsi partendo da 1 campione alla volta ed arrivare a n campioni utili. Fino a quando non si raggiunge un superamento dell'isteresi, ovvero il tempo di risposta per cui il sistema interviene.

>Tentativi fatti da un terzo dell'ampiezza dichiarata dell'attuatore, mettendosi a 5/6 watt, appena raggiunto un equilibrio temporale, fare l'ampiezza dell'attuatore.

Equazione in wattaggio fare in Volt e per passare in watt bisogna conoscere l'impedenza.

Mettere in coda un analizzatore di spettro, mettendolo nel circuito tramite un microfono.

Usare mettendo a massimo 30 cm con OM1 con al massimo della dinamica, a 7 cm quando si è ad un terzo della potenza dell'attuatore.

Campagna di analisi realizzata per:
- impulsi singoli
- treni di impulsi (scarica da 10/11 impulsi equidistanziati tra loro di 50 ms massimo di differenza anche 20 ms)

>Treno di impulsi deve poter permettere la diversificazione del damping delle risonanze.

Riconoscere un'identità temporale della cassa armonica, oltre che a quella frequenziale.

Tempo di decadimento e modalità in cui queste avvengono.

Poi si avrà da capire con Earthworks.

### Maria Vittoria Agresti

- inserire dose di Live Electronics...
- chitarra 8 corde, con corda che si può scordare
- non riconoscibilità del suono o della sorgente stessa
- indagare sul suono e sulla sua natura e renderlo non associabile ad una sorgente
- approfondire l'uso dei filtri, per bande, e comb

Convergenza sulla chitarra...

Difficoltà sulle 8 corde, e sull'emettitore con le corde.

Indagine del suono originale, non escludendo l'emettitore altoparlante in prossimità e ben integrabile con il suono dello strumento acustico.

Ama le frequenze gravi...

Utilizzo di altoparlante con guida d'onda per prevenire il feedback.

---

Esercizi sull'elaborazione ed integrazione dell'elettronica con il suono della chitarra.

Bisogno della campionatura dello strumento ad accordatura reale.

Lasciar risuonare le corde, senza introduzione di vincoli sulle corde.

Emulazione e simulazione.

- Emulazione
- filtraggio
- processi temporali

>Tutti esercizi con un live

---

## Riepilogo

- esercizi sul Live Electronics con diagramma di flusso (Vittoria file audio per ogni corda e Manuela esercizio)
- Roberto -> tesi, progettare sulla tesi
- individuare un analizzatore di spettro

## Cloud

Account generico per il cloud dal sito per tutti i ragazzi del corso.
