# Approdi al Feedback, per chitarra classica aumentata e Live Electronics

__Litus__

## Accordatura iniziale

- prima corda __D4__ 293,67 Hz
- seconda corda __A3__ 220 Hz
- terza corda __A3__ 220 Hz
- quarta corda __D3__ 146,83 Hz
- quinta corda __A2__ 110 Hz
- sesta corda __D2__ 73,416 Hz

## ...dal profondo...

_chitarra appoggiata sulle cosce dell'interprete con la buca rivolta verso il basso, stoppando le corde_

- dal nulla con __BSX+$\delta$__ <p style="color:red;">(Filtro per BSX a frequenza più bassa possibile)</p>
- elemento profondo e sinusoidale si presenta da puro (elemento lineare e senza vibrazioni di sorta, l'attuatore è tenuto premuto dall'interprete)
  - vi sono vari crescendi e diminuendi (_<><><_) che non arrivano mai al nulla
- riappare lo stesso elemento profondo con intensità maggiore (regolata dal pedale)
- l'intensità maggiore diviene vibrazione (l'attuatore è lasciato vibrare dall'interprete senza essere tenuto)
  - la posizione dell'attuatore viene via via aggiustata se esso si muove dalla posizione in cui esso deve essere
  - realizzare movimenti che arrivino all'apice dei crescendi a fortissimi (sempre come _<><><_), è importante che l'attuatore venga lasciato vibrare sui fortissimi
- far nascere altra altezza da __BSX+$\alpha$__ <p style="color:red;">(Spostamento del filtro se necessario)</p>
  - ripartendo dal nulla, giungere a qualcosa di udibile e aumentare con il pedale fino alla vibrazione dell'attuatore (facendo muovere l'attuatore)
  - prolungare la durata del feedback dominandolo
  - ripetere il gesto di crescendi e diminuendi
- giunti ad un fortissimo, lasciare risuonare la chitarra __allontanando il piezo dal corpo della chitarra__

<p style="color:red;">(Inserire nella risonanza tape che entra su EX80s)</p>

## limen (prima transizione)

- piezo su $\gamma$ feedback __BSX+$\gamma$__ <p style="color:red;">(Utilizzando lo stesso filtro usato precedentemente)</p>
  - crescere fino a un mf
  - rimanere statici su mf
  - arrivare al f e stoppare il feedback alzando l'attuatore dal corpo della chitarra
- __BSX+$\alpha$__ subito con vibrato (ff)
  - facendo suonare immediatamente le corde con il ff dell'attuatore <p style="color:red;">(la vibrazione deve essere un motivo ed un suono simile a quello che deve essere introdotto sugli EX80s con tape di registrazioni di feedback e sinusoidi che si mescolano al resto)</p>
- <p style="color:red;">continuano a suonare in regioni gravi gli EX80s</p>; BSX e piezo sono posti su dei tavolini al lato dell'interprete

- si iniziano a realizzare movimenti con le unghie di entrambe le mani sul nastro di carta del dorso della chitarra
  - prima lenti ed orizzontali (solo sulle strisce orizzontali)<p style="color:red;">(su cui vai a sfumare il tape dopo un morphing a suoni simili a quelli degli strusciati con le unghie)</p>
  - i movimenti, rimanendo orizzontali, sono lenti in un senso e molto velocemente si chiudono con un gesto nella direzione opposta
  - soffermandosi su punti specifici del retro del corpo della chitarra, i movimenti che vengono realizzati sono sempre più su spazi molto piccoli, veloci e circoscritti
  - tra i movimenti brevi si aggiungono dei movimenti verticali (sui nastri di carta verticali) sino ad arrivare ad un rallentamento dei movimenti che restano inizialmente casualmente verticali ed orizzontali
  - ci si sofferma poi su movimenti __verticali-orizzontali-picchiettando la fascia a mo di percussione__
  - si segue invertendo i primi due __orizzontali-verticali-fascia percossa__
  - __fascia-orizzontali-fascia__
  - questi ultimi tre gesti si devono ripetere sino ad arrivare ad un maggior numero di elementi percussivi sulla fascia e nascondendo via via gli altri elementi con le unghie

- rimangono solo elementi percussivi sulla fascia con la mano destra

## limes (seconda transizione)

- lo pseudo-ritmo introdotto dagli elementi percussivi sulla fascia, viene ripreso dal tapping stoppato sui tasti __IV-V-VI__ della quarta, quinta e sesta corda <p style="color:red;">(Gli elementi percussivi precedenti e questi del tapping vengono elaborati in live da DELRM e altri delay e inviati su EX80s; probabilmente necessari microfoni esterni non solo per l'amplificazione in sala ma per l'elettronica)</p>
- gli elementi del tapping risuonano prima negli EX80s elaborati e processati, poi suoni simili ma molto più complessi suonano in sala <p style="color:red;">(apertura verso la sala dei suoni dallo strumento)</p>
  - bisogna cercare di realizzare una sorta di passaggio in cui l'interprete diviene l'eccitatore della sala ed interagisce con la stessa
  - quindi un evento sul palco, passa da interprete al fronte, poi l centro della sala e poi sulla parte posteriore (da capire come)
  - dopo due o tre eccitazioni con elementi di tapping con dinamiche forti e fortissime la sala suona e risuona da sola
- quando i due interpreti convengono che la sala e gli EX80s suonano da soli, __l'interprete alla chitarra, in maniera discreta e facendo comprendere teatralmente il gesto, gira lo strumento e lo pone in posizione tradizionale con un poggiapiedi sotto il piede sinistro e il pedale sotto il piede destro__

## ...alla superficie...

_chitarra in posizione tradizionale con il pedale sotto il piede destro_

- l'elettronica che ora è solamente in sala, si trasforma, subisce una mutazione
- l'elettronica affievolisce e scompare dalla sala gradualmente, rimarcando che una parte di essa provenga solamente dall'attuatore facendo un passaggio dagli EX80s al Tectonic
- vi sono ora armonici sparsi sui tasti __IV-V-VI__ della __quarta, quinta e sesta corda__, in maniera non rigorosa alcuni di essi sono tapping, sugli stessi tasti; utilizzando sia la mano destra che quella sinistra (in questa nuova veste il tapping non è stoppato) <p style="color:red;">(insieme a dei gesti molto tenui sul Tectonic, gli elementi suonati dal chitarrista sono amplificati con microfoni dedicati)</p>
- rimanendo il tapping con la sola mano sinistra, la mano destra impugna il piezo, attendendo fino al momento in cui il tapping è pronto per realizzare l'introduzione del Feedback
- il piezo nella mano destra viene posizionato esitando su __F__
  - questi gesti contemporanei al tapping devono far percepire solo minimamente la presenza di un'altezza, che appena percepita scompare
- arrivando al trillo con il tapping viene aggiunto <p style="color:red;">il feedback con la AM</p>
  - <p style="color:red;">la AM oscilla e il tapping deve andare a sincronizzarsi con essa</p>
- quando il tapping è sincronizzato con l'oscillazione del Feedback, si arriva ad un fortissimo che porta a __staccare il piezo dal corpo della chitarra__ <p style="color:red;">venendo gradualmente rimossa l'amplificazione della chitarra</p>
- rimane il solo feedback della chitarra con AM
- dall'AM si passa gradualmente ad un feedback puro
- cambiano 3/4 posizioni si attraversano questi stadi
  - cambiando le posizioni si devono percepire frequenze uguali o vicine
  - percezione di frequenze diverse con lo stesso punto <p style="color:red;">(realizzato tramite filtri o tramite posizionamento diverso del piezo)</p>
  - si mantiene fissa una frequenza (come 210 Hz) e si scordando alcune corde per farle entrare in battimento <p style="color:red;">(ricordarsi che il battimetno si può realizzare anche aggiungendo gli EX80s)</p>
  - instaurati i battimenti, si devono far percepire gli armonici sfiorando i tasti con le corde che si muovono per simpatia (sorta di anticipazione degli armonici diagonali che ci saranno successivamente)
  - utilizzare per far percepire gli armonici, le vibrazioni delle corde e per legare i passaggi con la mano sinistra dell'oggettistica come:
   - bottleneck
   - coltello di plastica
   - matita o penna
- il piezo viene quindi staccato dal corpo ultimando con un fortissimo <p style="color:red;">rimanendo in sala la risonanza artificiale dello stesso</p>
- <p style="color:red;">avviene un morphing spettrale della risonanza che diviene bordone per gli armonici in diagonale sull'impianto in sala</p>
- si iniziano a percepire dal nulla __armonici singoli sulla quinta e sulla sesta corda al V e VI tasto__ <p style="color:red;">amplificando molto la chitarra in sala e facendo si che i suoni degli armonici risuonino realizzando un tappeto sonoro</p>
  - n.b. è importante che l'interprete alla chitarra suoni tali armonici in maniera molto diradata, facendo si che essi inizialmente non facciano percepire alcun gesto melodico puramente tonale
- si iniziano a realizzare le prime __diagonalizzazioni degli armonici, arrivando a IV e III tasto__ <p style="color:red;">
- scritte come accordi arpeggiati con le note come diamanti
- salendo sempre di più con gli armonici in diagonale, toccando __XI, XIII, XII, XV tasto__
- si arriva ad un'interazione tra armonici singoli e diagonalizzazioni degli stessi
- fino ad arrivare ad un solo finale con elettronica ma solo con amplificazione su __III e V armonico in viola con nastro carta sulla buca__
- _è importante per questa parte finale cercare il vuoto in sala lasciando la sola chitarra_
  - non rimane nulla se non la chitarra amplificata
  - si può inizialmente esagerare con un riverbero
  - rendendolo alla fine del brano sempre meno presente ed asciugando la sala, con suono intimo e personale

---

Dove non specificato altrimenti la posizione della chitarra rimane quella dell'indicazione che precede il gesto da compiere, esempio: _chitarra appoggiata sulle cosce dell'interprete con la buca rivolta verso il basso, stoppando le corde_, vale sino alla nuova indicazione.

---

Versione del 16 Agosto 2024 19:00
