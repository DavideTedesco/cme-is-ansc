<CsoundSynthesizer>
<CsOptions>
; Select audio/midi flags here according to platform
-odac            ; Write sound to the output device
</CsOptions>
<CsInstruments>
; Initialize the global variables.
sr = 96000        ; Sample rate
ksmps = 32        ; Control rate
nchnls = 2        ; Number of output channels
0dbfs = 1         ; Maximum amplitude is 1


; Define the instrument
instr 1
    ; Carrier frequency and amplitude
    icfreq = p4    ; Carrier frequency (Hz)
    iamplitude = p5; Amplitude (0 to 1)

    ; Modulator frequency and index
    imodfreq = p6  ; Modulator frequency (Hz)
    imodindex = p7 ; Modulation index

    ; Generate the modulator signal
    amod = oscili(imodindex, imodfreq, 1)

    ; Apply an envelope to the amplitude
    aenv = linen(iamplitude, 0.3, p3, 0.3)  ; Simple linear envelope with 0.1s attack and release

    ; Generate the carrier signal, amplitude modulated by the modulator signal
    acar = oscili(aenv * (1 + amod), icfreq, 1)

    ; Output the signal
    outs acar, acar
endin
</CsInstruments>
<CsScore>
; Table for the oscillators
f1 0 8192 10 1    ; Sine wave

; Play the instrument
; p1  p2  p3  p4  p5    p6     p7
; ins st  dur freq amp modfreq modindex
;i1  0    30   300  0.5   1      0.5
;i1  0   30   300  0.5   2      0.5
;i1  0   30   300  0.5   3     0.5
;i1  0   30   300  0.5   4     0.5
;i1  0   30   300  0.5   5     0.5
;i1  0   30   300  0.5   5.5     0.5
;i1  0   30   300  0.5   6     0.5
;i1  0   30   300  0.5   7     0.5
i1  0   30   300  0.5   8     0.5

; End the score
e
</CsScore>
</CsoundSynthesizer>
