import("stdfaust.lib");

impulseD = mem(1),1:> _,_ : -;
//process = button("impulse")*impulseD;

//process = os.pulsetrainN(freq,duty);

//n = hslider("n",1,1,20,0.1);;

//process = os.pulsetrainN(1,freq,duty);

interval = hslider("interval (ms)",2000,1,10000,1) : ba.sec2samp/1000;
length = hslider("length (samps)",10,0.01,300,0.00001);

process =  checkbox("ON/OFF")*ba.pulsen(length,interval) : _;
