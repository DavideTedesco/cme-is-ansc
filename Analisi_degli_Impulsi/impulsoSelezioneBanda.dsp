import("stdfaust.lib");


interval = hslider("interval (ms)",2000,1,10000,1) : ba.sec2samp/1000;
length = hslider("length (samps)",10,0.01,300,0.00001);
volume = hslider("volume",0.3,0.01,1,0.00001);


trenoDiImpulsi =  checkbox("ON/OFF")*ba.pulsen(length,interval) : _;

N = 4;
/*
E: 82 Hz (E2 Musical Note)

A: 110 Hz (A2 Musical Note)

D: 147 Hz (D3 Musical Note)

G: 196 Hz (G3 Musical Note)

B: 247 Hz (B3 Musical Note)

E: 330 Hz (E4 Musical Note)
*/
e2 = 82;
a2 = 110;
d3 = 147;
g3 = 196;
b3 = 247;
e4 = 330;

filterFrequency = e4;

filtri = fi.lowpass(N,filterFrequency) : fi.highpass(N,filterFrequency);

process = trenoDiImpulsi : filtri : _*volume ;
