import("stdfaust.lib");


interval = hslider("interval (ms)",400,1,10000,1) : ba.sec2samp/1000;
length = hslider("length (samps)",10,0.01,300,0.00001);
volume = hslider("volume",0.8,0.01,10,0.00001);


trenoDiImpulsi =  checkbox("ON/OFF")*ba.pulsen(length,interval) : _;

N = 4;
/*
E: 82 Hz (E2 Musical Note)

A: 110 Hz (A2 Musical Note)

D: 147 Hz (D3 Musical Note)

G: 196 Hz (G3 Musical Note)

B: 247 Hz (B3 Musical Note)

E: 330 Hz (E4 Musical Note)
*/
e2 = 82;
a2 = 110;
d3 = 147;
g3 = 196;
b3 = 247;
e4 = 330;

filterFrequency1 = e2;
filterFrequency2 = a2;
filterFrequency3 = d3;
filterFrequency4 = g3;
filterFrequency5 = b3;
filterFrequency6 = e4;

filtri1 = fi.lowpass(N,filterFrequency1) : fi.highpass(N,filterFrequency1);
filtri2 = fi.lowpass(N,filterFrequency2) : fi.highpass(N,filterFrequency2);
filtri3 = fi.lowpass(N,filterFrequency3) : fi.highpass(N,filterFrequency3);
filtri4 = fi.lowpass(N,filterFrequency4) : fi.highpass(N,filterFrequency4);
filtri5 = fi.lowpass(N,filterFrequency5) : fi.highpass(N,filterFrequency5);
filtri6 = fi.lowpass(N,filterFrequency6) : fi.highpass(N,filterFrequency6);
f1 = hslider("f1",1,0.01,1,0.00001);
f2 = hslider("f2",1,0.01,1,0.00001);
f3 = hslider("f3",1,0.01,1,0.00001);
f4 = hslider("f4",1,0.01,1,0.00001);
f5 = hslider("f5",1,0.01,1,0.00001);
f6 = hslider("f6",1,0.01,1,0.00001);


process = trenoDiImpulsi <: filtri1*f1, filtri2*f2, filtri3*f3, filtri4*f4, filtri5*f5, filtri6*f6 :> _*volume ;
