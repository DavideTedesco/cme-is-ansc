# Analisi degli Impulsi

Questa cartella contiene i file sorgente per la realizzazione di impulsi di varia natura e durata per lo studio degli stessi sulla chitarra classica.

I file audio e i progetti relativi a tali analisi sono reperibili [qui](https://www.davidetedesco.it/vault/index.php/s/e95Dpm4Ff7Gna58).
