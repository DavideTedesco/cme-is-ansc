# C.A.G.E. - Classical Augmented Guitar Equipment

C.A.G.E. è un agglomerato di tecnica, tecnologia ed acustica applicato ad una chitarra classica. La chitarra è classica e non acustica, la quale si differenzia per la tipologia di corde utilizza, la prima annovera infatti corde in nylon per i cantini mentre la seconda è tipicamente equipaggiata con i cantini in materiale metallico. Oltre a tale differenza le chitarre acustiche hanno una diversa tipologia di costruzione, cultura e storia, ben separata dalle chitarre classiche.

L'aumentazione della chitarra classica è in questo caso modulare e pertanto non è cristallizzata e permanentemente immutabile. Nella veste qui presentata il processo di aumentazione è realizzato tramite attuatori (exciters dall'inglese), sensore piezo-elettrico e due piccolo sub-woofer.

Gli attuatori, seguendo un principio di funzionamento simile a quello degli altoparlanti, sfruttano la trasduzione per mettersi in vibrazione ed ancorati e fissati a diversi materiali permettono di farci percepire la loro natura vibrazionale nascosta senza quegli stessi materiali che mettono in vibrazione. Il sensore piezo-elettrico, sfruttando la vicinanza alla cassa armonica della chitarra permette di generare un feedback controllabile e modulabile grazie ad una componente hardware e software costruita ad hoc.

La gestione di ogni singolo elemento che viene ascoltato dal sistema è lasciata all'interprete, che grazie ad un pedale ne controlla il volume in ingresso. Il resto delle trasformazioni è fornito e modellato in tempo reale da un'interprete al Live Electronics che può gestire e modificare ciò che verrà generato dal sistema.

Davide Tedesco, 09/09/2024
